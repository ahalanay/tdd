"
Test for Counter class 
"
Class {
	#name : #CounterTest,
	#superclass : #TestCase,
	#category : #MyCounter
}

{ #category : #tests }
CounterTest >> testCountisSetandRead [
|c|
c := Counter new.
c count: 8.
self assert: c count equals: 8 
]

{ #category : #tests }
CounterTest >> testDecrement [
|c|
c := Counter new.
c count: 2;decrement;decrement.
self assert: c count equals: 0 
]

{ #category : #tests }
CounterTest >> testIncrement [
|c|
c := Counter new.
c count: 0;increment;increment.
self assert: c count equals: 2 
]

{ #category : #tests }
CounterTest >> testValueAtCreationisZero [
|c|
c := Counter new.
self assert: c count equals: 0 
]
