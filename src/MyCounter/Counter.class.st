"
Counter is a simple concrete class which supports incrementing and decrementing a
counter.
Its API is
- decrement, increment
- count
Its creation API is message withValue:

"
Class {
	#name : #Counter,
	#superclass : #Object,
	#instVars : [
		'count'
	],
	#category : #MyCounter
}

{ #category : #accessing }
Counter >> count [
"return the current value of the value instance variable"
^ count

]

{ #category : #accessing }
Counter >> count: aNumber [
"Sets counter equal to aNumber"
   count := aNumber
]

{ #category : #accessing }
Counter >> decrement [ 
"return the current value of the value instance variable"
count:=count - 1

]

{ #category : #accessing }
Counter >> increment [ 
"return the current value of the value instance variable"
count:=count + 1

]

{ #category : #initialization }
Counter >> initialize [ 
"Initializes counter with value 0"
count := 0
]

{ #category : #printing }
Counter >> printOn: aStream [ 
    super printOn: aStream. 
    aStream nextPutAll: ' with value: '; nextPutAll: self count printString.


]
