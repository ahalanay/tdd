"
""Example Tests for Sets""

"
Class {
	#name : #MyExampleSetTest,
	#superclass : #TestCase,
	#instVars : [
		'full',
		'empty'
	],
	#category : #'MySet-Tests'
}

{ #category : #running }
MyExampleSetTest >> setUp [
	"Hooks that subclasses may override to define the fixture of test."
	empty := Set new.
	full := Set with:5 with:6

]

{ #category : #tests }
MyExampleSetTest >> testAddTwice [

|s|
s:=Set new.
self assert: s isEmpty.
s add: $A.
self assert: s size equals: 1.
s add: $A.
self assert: s size equals: 1.

]

{ #category : #tests }
MyExampleSetTest >> testIncludes [

self assert: (full includes: 5).
self assert: (full includes: 6).

]

{ #category : #tests }
MyExampleSetTest >> testOccurences [ 

full := Set with: 5 with: 6.
self assert: (empty occurrencesOf: 1) equals: 0.
self assert: (full occurrencesOf: 5) equals: 1.
full add: 5.
self assert: (full occurrencesOf: 5) equals: 1.


]

{ #category : #tests }
MyExampleSetTest >> testRemove [ 
"self run: #testRemove"
full remove: 5.
self assert: (full includes: 6).
self deny: (full includes: 5)


]
